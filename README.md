--------------------------------------------------
|                BRANCHE MAIN                    |
--------------------------------------------------
Le reste sera fait sur la branche DEV

--------------------------------------------------
|                CONFIGURATION                   |
--------------------------------------------------
- PHP 7.4.26
- MySQL 8.0.27
- Symfony 5.4

--------------------------------------------------
|                 UTILISATEURS                   |
--------------------------------------------------
- Login: rayhan.ghezzi@gmail.com       mdp: adminadmin        roles: USER, ADMIN
- Login: rayhan.test@gmail.com         mdp: testtest          roles: USER

--------------------------------------------------
|                   NOTES                        |
--------------------------------------------------
. Vous trouverez ce que j'ai fais sur cette branche en un peu plus de 2h30 en prenant en compte l'initialisation du projet

. Elle contient :
- Création d'un compte
- Authentification
- Entités "User" et "UserRequests"
- Création d'une requête
- Affichage des requêtes