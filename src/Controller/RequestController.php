<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserRequest;
use App\Form\RequestFormType;
use App\Services\UserRequestService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RequestController extends AbstractController
{
    /**
     * @Route("/request", name="index_request")
     */
    public function index(UserRequestService $userRequestService): Response
    {
        $usersRequests = $userRequestService->getAllUsersRequests();
        
        return $this->render('request/index.html.twig', [
            'usersRequests' => $usersRequests,
        ]);
    }

    /**
     * @Route("/new/request", name="new_request")
     */
    public function newUserRequest(Request $request, EntityManagerInterface $entityManager): Response
    {
        $user           = $this->getUser();
        $userRequest    = new UserRequest();
        $form           = $this->createForm(RequestFormType::class, $userRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRequest->setUserReference($user);

            $entityManager->persist($userRequest);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('request/new_request.html.twig', [
            'requestForm' => $form->createView(),
        ]);
    }
}
