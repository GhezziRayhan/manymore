<?php

namespace App\Services;

use App\Entity\UserRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class UserRequestService 
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function getAllUsersRequests(): array
    {
        $usersRequests = $this->entityManager->getRepository(UserRequest::class)->findAll();

        return $usersRequests;
    }

}